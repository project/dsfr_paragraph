import { defineConfig } from 'vite'
import { resolve } from 'path'
import cleanPlugin from 'vite-plugin-clean'

const output = '../dist/'
const assets = ''
const isWatch = process.argv.includes('--watch')

const style = 'src/style/'

const inputs = {

  dsfr_paragraph: resolve(__dirname, style+'dsfr_paragraph.scss'),
}

// https://vitejs.dev/config/
export default defineConfig(({mode}) => ({
  base: './',
  root: './src',
  plugins: [
    cleanPlugin({
      targetFiles: [output]
    }),
  ],
  build: {
    minify: mode === 'development' || isWatch === true ? false : 'terser',
    emptyOutDir: true,
    outDir: output,
    sourcemap: false,
    cssCodeSplit: true,
    rollupOptions: {
      input: inputs,
      output: {
        assetFileNames: assetInfo => {
          const info = assetInfo.name.split('.')
          const extType = info[info.length - 1]
          if (/\.(css)$/.test(assetInfo.name)) {
            return `${assets}css/[name].min.${extType}`
          }
          return `[name].${extType}`
        },
      },
    }
  }
}))