import fs from 'fs/promises';
import path from 'path';
import { fileURLToPath } from 'url';
import { exec } from 'child_process';
import { promisify } from 'util';

const execAsync = promisify(exec);
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const srcDir = path.join(__dirname, 'src/js');
const distDir = path.join(__dirname, 'dist/js');

async function buildJs() {

  try {

    // Ensure dist directory exists
    await fs.mkdir(distDir, { recursive: true });

    // Get all .es6.js files in src directory
    const files = await fs.readdir(srcDir);
    const es6Files = files.filter(file => file.endsWith('.es6.js'));

    for (const file of es6Files) {
      const srcFile = path.join(srcDir, file);
      const distFile = path.join(distDir, file.replace('.es6.js', '.min.js'));

      // Transpile with Babel
      try {
        await execAsync(`npx babel ${srcFile} --out-file ${distFile}`);
        console.log(`Transpiled ${file}`);

        // Minify with Terser
        await execAsync(`npx terser ${distFile} --compress --mangle -o ${distFile}`);
        console.log(`Minified ${file}`);
      } catch (error) {
        console.error(`Error processing ${file}: ${error}`);
      }
    }
  } catch (error) {
    console.error(`Build failed: ${error}`);
  }

}

buildJs();