# DSFR Paragpraph

## DSFR theme (and dsfr_core) required!

https://www.drupal.org/project/dsfr

dsfr_core will test whether the parent theme is active.

If you use the "/admin/dsfr/theme" link, you'll be redirected to the parent theme (if active).

## Install with Drush

```shell
drush en dsfr_paragraph -y
```

## Front elements

Install JS quickly:
```
npm install
```

Update style elements:
```
npm run build
```
