<?php

namespace Drupal\dsfr_paragraph;

use Drupal\Core\Controller\ControllerBase;

class ParagraphStorage extends ControllerBase
{
  /**
   * {@inheritdoc}
   * Use for ready-to-use paragraphs for nodes
   */
  public function typeOperatingParagraphs() {

    $checkbox_settings = ['format' => 'boolean'];
    $conf_color = [ 'required' => 'true', 'default' => 'color-default' ];
    $media_image_settings = [
      'handler' => 'default:media',
      'handler_settings' => ['target_bundles' => [ 'image' => 'image' ]]
    ];
    $required = [ 'required' => 'true' ];

    $types = [

      'layout' => [ //'design/table-line
        'desc' => 'Layout available to accommodate components.',
        'order' => 0,
        'plugins' => [
          'layout_paragraphs' => [
            'enabled' => true,
            'available_layouts' => [
              'layout_onecol' => $this->t('One column'),
              'layout_twocol' => $this->t('Two column'),
              'layout_twocol_25_75' => $this->t('Two column 25/75'),
              'layout_twocol_40_60' => $this->t('Two column 40/60'),
              'layout_twocol_60_40' => $this->t('Two column 60/40'),
              'layout_twocol_75_25' => $this->t('Two column 75/25'),
              'layout_twocol_bricks' => $this->t('Two column bricks'),
              'layout_threecol_25_50_25' => $this->t('Three column 25/50/25'),
              'layout_threecol_33_34_33' => $this->t('Three column 33/34/33'),
              'layout_threecol_top_25_50_25_bottom' => $this->t('Three column 25/50/25'),
            ]
          ]
        ],
      ],

      // ------------------------------------------------------------------------------------------------------------------------- //
      // Content edition
      'ckeditor'      => [ //'design/edit-box-line'
        'label' => 'Text editor',
        'desc'  => 'CKEditor text editor offering a range of advanced features for creating and editing text content.',
        'order' => 1,
        'fields'=> ['ckeditor5' => $required ]
      ],
      'title'         => [
        'label' => 'Intertitle',
        'order' => 10,
        'fields'=> [
          'title' => $required,
          'title_type'  => [ 'required' => 'true', 'default' => '2'],
          'title_style' => [ 'required' => 'true', 'default' => '0'],
        ]
      ],
      'highlight'     => [ //'design/mark-pen-line'
        'desc'  => 'Highlighting enables the user to distinguish and locate information easily. Highlighting highlights information that is already present in the rest of the content: either a reformulation or a repetition of the text.',
        'order' => 15,
        'fields'=>  [ 'message' => $required, 'color' => $conf_color ],
      ],

      'image'         => [ 
        'desc'  => '...',
        'order' => 20,
        'fields'=>  [ 
          'media_image' => [ 'settings' => $media_image_settings ],
          'legend' => [], 
          'size' => ['default' => 0] 
        ],
      ],

      'accordions'    => [
        'order' => 30,
        'para'  => 'accordion'
      ],
      'accordion'     => [
        'order' => 31,
        'fields'=> [ 'title' => $required, 'ckeditor5' => $required ],
        'layout'=> false
      ],
      'tabs'    => [
        'order' => 35,
        'para'  => 'tab'
      ],
      'tab'     => [
        'order' => 36,
        'fields'=> [ 'title' => $required, 'ckeditor5' => $required ],
        'layout'=> false
      ],

      'quote'         => [ //'editor/fr--quote-line'
        'desc'  => 'A quotation allows you to cite a text in an editorial page. The quotation can come from an extract of an oral speech by a third party, or from a written text.',
        'order' => 50,
        'fields'=> [
          'text' => $required,
          'author' => [],
          'color' => $conf_color,
          'details' => [],
        ]
      ],

      'link'          => [  //'business/links-line',
        'desc'  => 'The link enables navigation between a page and other content within the same page, on the same site or external to it.',
        'order' => 59,
        'fields'=> [ 
          'link' => $required,  
          'arrow_icon' => [], 
          'title' => [],
          'tooltip' => [],
          'font_size' => ['default' => 0]
        ]
      ],

      // ------------------------------------------------------------------------------------------------------------------------- //
      // Content collection

      // ------------------------------------------------------------------------------------------------------------------------- //
      // Information
      'callout'       => [ //'system/error-warning-line'
        'desc'  => 'Callout allows the user to quickly distinguish information that complements the content consulted.',
        'order' => 70,
        'fields'=> [
          'title' => [],
          'message' => [ 'required' => 'true' ],
          'link' => [],
          'color' => $conf_color,
        ]
      ],
      'alert'         => [ //'system/fr--warning-line'
        'desc'  => "Alerts can be used to draw the user's attention to a piece of information without interrupting the task in hand.",
        'order' => 75,
        'fields'=> [
          'alert' => [ 'required' => 'true', 'default' => 'warning' ],
          'title' => [],
          'message' => [],
          'compact' => [ 'view_settings' => $checkbox_settings ],
          'close' => [ 'view_settings' => $checkbox_settings ],
        ]
      ],
      'badges'        => [
        'order' => 80,
        'para'  => 'badge'
      ],
      'badge'         => [
        'order' => 81,
        'fields'=> [
          'badge_type' => [ 'required' => 'true', 'default' => 'new' ],
          'label' => $required,
          'small' =>  [ 'view_settings' => $checkbox_settings , 'default' => 0 ],
          'show_icon' =>  [ 'view_settings' => $checkbox_settings, 'default' => 1 ],
          'color' => $conf_color
        ],
        'layout'=> false
      ],
      'tooltip'       => [ //'system/question-line',
        'order' => 85,
        'fields'=> [ 'tooltip' => $required ]
      ],

      // ------------------------------------------------------------------------------------------------------------------------- //
      // Other

      'block'         => [
        'label' => 'Drupal blocks',
        'desc'  => 'Provides a field for adding custom block instances.',
        'order' => 500,
        'fields'=> [ 'block' => $required ]
      ],

      'tags'          => [
        'order' => 505,
        'para'  => 'tag'
      ],
      'tag'           => [
        'order' => 506,
        'fields'=> [
          'link' => $required,
          'small' =>  [ 'view_settings' => $checkbox_settings , 'default' => 0 ],
          'arrow_left' => [],
          'tag_type' => [ 'default' => 0 ]
        ],
        'layout'=> false
      ],

      'code'         => [ //
        'order' => 510,
        'fields'=> [
          'code_type' => [ 'default' => 'markup'],
          'text' => $required,
        ]
      ],      
    ];    

    foreach( $types as $key => $row ) {

      $types_para[$key] = [
        'label' => $this->t(( array_key_exists('label', $row)) ? $row['label'] : ucfirst($key)),
        'weight'=> $row['order']
      ];

      $types_para[$key]['desc'] = ( array_key_exists('desc', $row) ) ? $row['desc']: '';
      $types_para[$key]['fields'] = [];
      if( array_key_exists('fields', $row) ) {
        if( $key !== 'layout' ) { $types_para[$key]['fields']['margin_top'] = []; }
        $types_para[$key]['fields'] = array_merge( $types_para[$key]['fields'], $row['fields'] );     
      }
      $types_para[$key]['plugins'] = ( array_key_exists('plugins', $row) ) ? $row['plugins'] : [];      

      if( array_key_exists('layout', $row) ) {
        
        $types_para[$key]['layout'] = $row['layout'];
        $types_para[$key]['icon'] = false;

      } else {

        $types_para[$key]['icon'] = true;

        if( array_key_exists('para', $row) ) {
          $types_para[$key]['fields'][$key] = [ 
            'paragraph' => 'true',
            'settings' => [
              'handler' => 'default:paragraph',
              'handler_settings' => [
                'target_bundles' => ['dsfr_'. $row['para'] => 'dsfr_'. $row['para']],
              ]
            ]        
          ];
        }

        if( $key !== 'layout' ) { $types_para[$key]['fields']['margin_bottom'] = []; }         
      }
    }

    return $types_para;
  }
}