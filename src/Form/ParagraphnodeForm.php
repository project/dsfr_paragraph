<?php
/**
 * @file
 * Contains \Drupal\dsfr_paragraph\Form\ParagraphnodeForm.
 */
namespace Drupal\dsfr_paragraph\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;

class ParagraphnodeForm extends FormBase {

  public $field_name = 'field_dsfr_paragraph_layout';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dsfr_paragraph_node';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // ------------------------------------------------------------------------------------------------------------ //
    // Get themes info
    
    $tools = \Drupal::service('dsfr_core.tools');
    $theming = $tools->checkTheme();

    $tools->msg('Please note that, to improve the user experience, the admin theme will be automatically disabled for content creation (article, page, etc.). To reactivate the admin theme, go to the “/admin/appearance” page.', 'warning'); 

    if( $theming['missing'] == true ) { $tools->msg($this->t('The DSFR theme does not seem to be present in your theme list.'), 'warning'); }
    
    // ------------------------------------------------------------------------------------------------------------ //
    // Get node types available

    $nodes_type = $options = $options_attributes = [];

    $entity_type_manager = \Drupal::service('entity_type.manager');
    $node_storage = $entity_type_manager->getStorage('node_type');

    // Get all node type definitions
    $node_types = $node_storage->loadMultiple();

    // Loop through each node type definition
    foreach ($node_types as $node_type) {

      $tnid = $node_type->id();
      $nodes_type[$tnid]['label'] = $options[$tnid] = $node_type->label();
      
      // Check is the field is already in the node type
      $field_storage = \Drupal::entityTypeManager()
      ->getStorage('field_storage_config')
      ->load("node.$this->field_name");

      if ($field_storage && in_array($tnid, $field_storage->getBundles())) 
      { $options_attributes[$tnid] = ['disabled' => 'disabled']; } 
    }

    // ------------------------------------------------------------------------------------------------------------ //
    // Show form

    //$form['#attributes']['class'] = ['card', 'card__content-wrapper'];

    $form['options_paragraph_node'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select node types using DSFR paragraphs component layout'),
      //'#description' => '...',
      '#options' => $options,
      '#options_attributes' => $options_attributes
    ];

    $form['DSFR_missing'] = [
      '#type' => 'hidden',
      '#value' => $theming['missing']
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Associate')
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get DSFR services
    $manage = \Drupal::service('dsfr_paragraph.paragraphManage');
    $tools = \Drupal::service('dsfr_core.tools');

    if ( $form_state->getValue('options_paragraph_node') ) {

      extract( $form_state->getValues() );

      $field_storage = FieldStorageConfig::loadByName( 'node', 'field_dsfr_paragraph_layout' );

      $para_dsfr_types = \Drupal::service('dsfr_paragraph.paragraphStorage')
      ->typeOperatingParagraphs();

      foreach( $options_paragraph_node as $node_type_id => $value ) {
        
        if( $value != 0 ) {
          
          $manage->mergeFieldParaNode( 
            $node_type_id, 
            $field_storage,
            $para_dsfr_types,
            $field_storage
          );
        }
      }
    }

    $edit_node_admin = \Drupal::configFactory()->get('node.settings')->get('use_admin_theme');
    if( $edit_node_admin == true ) {

      \Drupal::configFactory()->getEditable('node.settings')
      ->set('use_admin_theme', false)
      ->save();
    }

    $tools->msg('Successful association with node types', 'success'); 

    return true;
  }
}