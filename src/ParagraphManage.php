<?php

namespace Drupal\dsfr_paragraph;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\FileInterface;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphsTypeInterface;

class ParagraphManage extends ControllerBase {

  public $paraPrefix = 'dsfr_';
  public $fieldPrefix = 'field_dsfr_p_';
  public $field_name = 'field_dsfr_paragraph_layout';

  /**
   * {@inheritdoc}
   */
  public function paraInstall() {

    # Creation of DSFR paragrah types
    $para_dsfr_types = \Drupal::service('dsfr_paragraph.paragraphStorage')->typeOperatingParagraphs();
    $this->paraTypeLoop( $para_dsfr_types );

    // ------------------------------------------------------------------------------------------------------------ //
    # Create a field DSFR paragrah for node types

    $field_name = $this->field_name;
    $field_storage = FieldStorageConfig::loadByName( 'node', $field_name );

    if( $field_storage == NULL ) {

      // Create the field storage.
      $field_storage = FieldStorageConfig::create([
        'field_name' => $field_name,
        'entity_type' => 'node',
        'type' => 'entity_reference_revisions',
        'cardinality' => -1, // Unlimited number of values
        'translatable' => true,
        'settings' => [
          'target_type' => 'paragraph', // The target entity type
        ],
      ]);
      $field_storage->save();
    }

    return $field_storage;
  }

  /**
   * {@inheritdoc}
   */
  public function paraTypeLoop( $para_dsfr_types ) {

    # Get ALL paragraphs type
    $para_all_type = ParagraphsType::loadMultiple();

    # Get fields parameters for DSFR
    $fs = \Drupal::service('dsfr_core.fieldStorage')->fieldData();

    # Icons
    $icons_path = __DIR__ . '/icons';
    // Create the directory for the icons
    $filesystem = \Drupal::service('file_system');
    $icon_upload_path = ParagraphsTypeInterface::ICON_UPLOAD_LOCATION;
    // Check the directory exists before writing data to it.
    $filesystem->prepareDirectory($icon_upload_path, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    
    foreach ( $para_dsfr_types as $id => $row ) {

      $dsfr_key = 'dsfr_' . $id;
      if(!array_key_exists($dsfr_key, $para_all_type)) { 

        extract($row);

        // ------------------------------------------------------------------------------------------------------------ //
        // Icon
        $slug = 'dsfr_' . $id . '.svg';
        $check_path = $icons_path . '/' . $slug;

        if( $icon && file_exists($check_path) ) { 

          $icon_setting = [
            'slug' => $slug,
            'path' => $check_path,
            'upload_path' => $icon_upload_path
          ];

        } else { $icon_setting = []; }

        // ------------------------------------------------------------------------------------------------------------ //
        // Create paragraphs type
        $this->paraTypeCreate($id, $label, $desc, $plugins, $icon_setting);

        // ------------------------------------------------------------------------------------------------------------ //
        // Manage fields for each paragraphs type
        if( count($fields) > 0 ) {
          
          foreach( $fields as $field_name => $params ) {
  
            if( array_key_exists( $field_name, $fs )) { 
              
              $field = $fs[$field_name]; 
              $form = (isset($field['display'])) ? $field['display'] : NULL;
              $view = (isset($field['view'])) ? $field['view'] : NULL;
  
              $field_storage = $this->paraFieldStorage( $field_name, $field );

              $conf = [
                'field_name' => $field_name,
                'label' => $field['label'],
                'form' => $form,
                'view' => $view
              ];

              if( array_key_exists( 'description', $field )) { $conf['description'] = $field['description']; }
              if( array_key_exists( 'required', $params )) { $conf['required'] = $params['required']; }
              if( array_key_exists( 'default', $params )) { $conf['default'] = ['value' => $params['default']]; }
              if( array_key_exists( 'default_callback', $params )) { $conf['default_callback'] = $params['default_callback']; }
              if( array_key_exists( 'settings', $params )) { $conf['settings'] = $params['settings']; }
              if( array_key_exists( 'view_settings', $params )) { $conf['view_settings'] = $params['view_settings']; }

              $this->paraFieldMerge(
                $conf,
                $field_storage,
                $id
              );
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function paraTypeCreate(
    $id, 
    $label, 
    $desc = NULL, 
    $plugins = [], 
    $icon = []) {
    
    $type = [
      'id' => $this->paraPrefix . $id,
      'label' => $label
    ];

    if( $desc != NULL || strlen($desc) > 0 ) { $type['description'] = $desc; }
    if( count($plugins) > 0 ) { $type['behavior_plugins'] = $plugins; }
    if( count($icon) > 0 && isset($icon['slug']) && $icon['slug'] != NULL ) { $type['icon_uuid'] = $this->getUuidFile( $icon ); }

    $paragraph_type = ParagraphsType::create($type);
    $paragraph_type->save();
  } 

  /**
   * {@inheritdoc}
   */
  public function paraFieldStorage( $name, $params ) {

    $field_name = $this->fieldPrefix . $name;
    $field_storage = FieldStorageConfig::loadByName( 'paragraph', $field_name );


    if( $field_storage == null ) {

      $storage = [
        'field_name' => $field_name,
        'entity_type' => 'paragraph',
        'type' => $params['type']
      ];

      if( array_key_exists( 'cardinality', $params )) { $storage['cardinality'] = $params['cardinality']; }
      if( array_key_exists( 'module', $params )) { $storage['module'] = $params['module']; }
      if( array_key_exists( 'settings', $params )) { $storage['settings'] = $params['settings']; }

      $field_storage = FieldStorageConfig::create($storage);
      $field_storage->save();
    }

    return $field_storage;
  } 

  /**
   * {@inheritdoc}
   */
  public function paraFieldMerge(
    $conf, 
    $field_storage, 
    $bundle   
  ) {

    $required = false;
    $settings = $default = $view_settings = [];
    $description = $default_callback = '';

    extract($conf);

    $field_config = [
      'field_storage' => $field_storage,
      'bundle' => $this->paraPrefix . $bundle,
      'label' => $label,
      'description' => $description,
      'required' => $required,
      'translatable' => true,
      'default_value' => $default,
      'default_value_callback' => $default_callback,
      'settings' => $settings,
    ];

    // Fieldin the type of paragraph
    FieldConfig::create($field_config)->save();

    $field_name = $this->fieldPrefix . $field_name;
    $fieldManage = \Drupal::service('dsfr_core.fieldManage');
  
    // Form Display
    $fieldManage->displayFields(
      'paragraph',
      $this->paraPrefix . $bundle,
      $field_name,
      $form
    );

    // Display
    $fieldManage->viewFields(
      'paragraph',
      $this->paraPrefix . $bundle,
      $field_name,
      $view,
      $view_settings
    );
  }

  /**
   * {@inheritdoc}
   */
  public function mergeFieldParaNode( 
    $node_type_id = 'article', 
    $field_storage = NULL,
    $para_dsfr_types = []
  ) {

    $entity_type_manager = \Drupal::entityTypeManager();
    $node_type = $entity_type_manager->getDefinition('node', $node_type_id);
    $field_name = $this->field_name;

    if( $node_type ) {

      $load = 'node.'.$node_type_id.'.default';

      // ------------------------------------------------------------------------------------------------------------ //
      // Check if the node type has the field
  
      if( $field_storage != NULL ) {
  
        $types = $drag_drop = $settings = $handler = [];

        $handler['target_bundles'] = NULL; 
        //$handler['bundles'] = NULL; 
        $handler['negate'] = 0; 

        if( count($para_dsfr_types) > 0 ) {

          foreach ( $para_dsfr_types as $id => $row ) { 

            $key = 'dsfr_' . $id;  
            
            if( isset($row['layout']) && $row['layout'] == false ) {

              $drag_drop[$key] = [
                'weight' => $row['weight'],
                'enabled' => false,
              ];
  
            } else {
              
              $types[$key] = $key;
              $drag_drop[$key] = [
                'weight' => $row['weight'],
                'enabled' => true,
              ];
            } 
          }
          
          $handler['target_bundles'] = $types;
          // $handler['bundles'] = $types;
          $handler['target_bundles_drag_drop'] = $drag_drop;
        }

        $settings['handler'] = 'default:paragraph';
        $settings['handler_settings'] = $handler;
    
        // Create the field instance.
        $field = FieldConfig::create([
          'field_storage' => $field_storage,
          'bundle' => $node_type_id, // The bundle (node type) for which the field is created
          'label' => $this->t('DSFR Components'),
          'settings' => $settings
        ]);
        $field->save();
      }

      // ------------------------------------------------------------------------------------------------------------ //
      // Form display

      $form_display = \Drupal::service('entity_type.manager')
      ->getStorage('entity_form_display')
      ->load($load);

      \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();

      if (!$form_display->getComponent($field_name)) {
        $form_display->setComponent($field_name, []);
      }
  
      $component = $form_display->getComponent($field_name);
      $component['type'] = 'layout_paragraphs'; //'entity_reference_revisions_widget';
      $component['weight'] = 1;
      $component['region'] = 'content';
      $component['settings'] = [
        'preview_view_mode' => 'default',
        'nesting_depth' => 0,
        'require_layouts' => 1,
        'empty_message' => ''
      ];
      $form_display->setComponent($field_name, $component);
      $form_display->save();
    
  
      // ------------------------------------------------------------------------------------------------------------ //
      // View Display

      $display_repository = \Drupal::service('entity_display.repository');
      $view_display = $display_repository->getViewDisplay( 'node', $node_type_id, 'default' );
      
      if (!$view_display->getComponent($field_name)) {
        $view_display->setComponent($field_name, []);
      }
      \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();
      //\Drupal::entityDefinitionUpdateManager()->applyEntityUpdate('node', $node_type);
      
      $view_display->setComponent( $field_name, [
        'type'  => 'layout_paragraphs',
        'label' => 'visually_hidden',
        'weight'=> 1,
        'region'=> 'content'
        ])
        ->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getUuidFile( $icon ) {

    $slug = $icon['slug'];
    $file_uri = $icon['upload_path'] . $slug;

    if ( file_exists($file_uri) ) {

      $uuid = $this->createIcon( $file_uri, $slug );

    } else {

      $filesystem = \Drupal::service('file_system');
      // TODO : change permissions is the folder already exists 
      $copied = $filesystem->copy($icon['path'], $icon['upload_path'], FileSystemInterface::EXISTS_REPLACE);

      if ( $copied ) { $uuid = $this->createIcon( $file_uri, $slug ); }      
    }

    if( $uuid != NULL) { return $uuid; }
  } 
    
  /**
   * {@inheritdoc}
   */
  public function createIcon( $file_uri, $slug ) {

    $file = File::create([
      'uri' => $file_uri,
      'uid' => \Drupal::currentUser()->id(),
      'name' => $slug,
      'filemime' => 'image/svg+xml',
      'status' => FileInterface::STATUS_PERMANENT,
    ]);
    $file->save();

    return $file->uuid->value;
  }
}